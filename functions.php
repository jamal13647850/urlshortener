<?php

/**
 * Created by PhpStorm.
 * User: Jamal
 * Date: 11/30/2018
 * Time: 11:20 AM
 */
class functions
{
    public function createForm(){
        echo <<<EOF
            <form action="" id="shortenerForm" method="POST">
                <div class="grid-container">
                    <div class="grid-x grid-padding-x">
                        <div class="medium-12 large-12 small-12 cell">
                            <label>آدرس اینترنتی:
                                <input type="url" placeholder="آدرس اینترنتی" name="urladdr" id="urladdr">
                            </label>
                        </div>
                    </div>
                    <div class="grid-x grid-padding-x">
                        <div class="medium-4 large-4 small-12 cell">
                        </div>
                        <div class="medium-4 large-4 small-12 cell">
                            <label>
                                <input type="submit" value="کوتاه کن" class="success button large" name="frmsbt" >
                            </label>
                        </div>
                        <div class="medium-4 large-4 small-12 cell">
                        </div>
                    </div>
                </div>
            </form>
EOF;

    }
    public function shortLink(){
        if(isset($_POST['frmsbt'])){
            $conn = $this->connect();
            $last_id = $this->insertUrl($conn,$_POST['urladdr']);
            if($last_id){
                echo "لینک شما با موفقیت کوتاه شد."."<br>";
                $shortLimk= (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"."?id=$last_id";
                echo $shortLimk;
            }
            else{
                echo "مشکلی پیش آمده است لطفا بعدا امتحان نمایید";
            }
        }
    }
    public function redirect(){
        if(isset($_GET['id'])){
            $conn = $this->connect();
            $url = $this->getUrlById($conn,$_GET['id']);
            if($url){
                $this->addClickCount($conn,$_GET['id'])
                ?>
                    <script>
                        location.href = "<?php echo $url; ?>";
                    </script>

                <?php
            }
        }
    }
    public function testConnection(){
        $servername = "localhost";
        $username = "root";
        $password = "";
        $dbname="university";
        try{
            $DBH = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
        }
        catch(PDOException $ex){

            die($ex->getMessage() );
        }
    }
    private function connect(){
        $servername = "localhost";
        $username = "root";
        $password = "";
        $dbname="university";

        try {
            $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
            // set the PDO error mode to exception
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            return $conn;
        }
        catch(PDOException $e)
        {
            echo "Connection failed: " . $e->getMessage();
        }
    }
    private function getUrlById($conn,$id){
        $stmt = $conn->prepare("SELECT url FROM urls where id=$id");
        $stmt->execute();

        // set the resulting array to associative
        $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
        $res = $stmt->fetchAll();
        if(isset($res[0])){
            $url = $res[0]['url'];
        }
        else{
            $url = false;
        }
        $conn = null;
        return $url;
    }
    private function insertUrl($conn,$url){
        $last_id = false;
        try {
            $sql = "INSERT INTO urls (url)
            VALUES ('$url')";

            $conn->exec($sql);
            $last_id = $conn->lastInsertId();
        }
        catch(PDOException $e)
        {
            //echo $sql . "<br>" . $e->getMessage();
            $last_id = false;
        }
        $conn = null;
        return $last_id;
    }
    public function addClickCount($conn,$id){
        $stmt = $conn->prepare("SELECT clicks FROM urls where id=$id");
        $stmt->execute();

        // set the resulting array to associative
        $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
        $res = $stmt->fetchAll();
        if(isset($res[0])){
            $clicks = $res[0]['clicks'];
            $stmt = $conn->prepare("UPDATE urls SET clicks=".($clicks+1)." WHERE id=$id");
            $stmt->execute();
        }
        else{
            $clicks = false;
        }
        $conn = null;
    }
}