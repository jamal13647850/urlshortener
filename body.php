<?php
/**
 * Created by PhpStorm.
 * User: Jamal
 * Date: 11/30/2018
 * Time: 11:06 AM
 */
$fn = new functions();


?>
<body style="background-color: lightgrey">
<?php
$fn->testConnection();
$fn->redirect();
?>
<div class="grid-container fluid" id="mainContainer">
    <h3>
        سلام <br> به کوتاه کننده لینک خوش آمدید
    </h3>
    <div class="grid-x grid-padding-x grid-padding-y">
        <div class="cell small-12 medium-4 large-4"></div>
        <div class="cell small-12 medium-4 large-4">
            <?php
                $fn->createForm();
            ?>
        </div>
        <div class="cell small-12 medium-4 large-4"></div>
    </div>
    <div class="grid-x grid-padding-x grid-padding-y">
        <div class="cell small-12 medium-4 large-4"></div>
        <div class="cell small-12 medium-4 large-4">
            <p style="font-size: 30px">
            <?php
                $fn->shortLink();
            ?>
            </p>
        </div>
        <div class="cell small-12 medium-4 large-4"></div>
    </div>

    <div class="grid-x grid-padding-x grid-padding-y">
        <div class="cell small-12 medium-6 large-6">
            <p style="font-size: 30px">
                <strong>
                    آی دی من در تمامی شبکه های اجتماعی
                <b style="color: brown">
                    jamal13647850
                </b>
                    هست.
                </strong>
            </p>
            <p style="font-size: 30px">
                خوشحال میشم با شما عزیزان در شبکه های اجتماعی بیشتر در ارتباط باشم.
            </p>
        </div>

        <div class="cell small-12 medium-6 large-6">
            <p style="font-size: 30px">
همچنین سورس این پروژه از طریق لینک زیر در دسترس عموم میباشد:
            </p>
            <p style="font-size: 30px">
                https://gitlab.com/jamal13647850/urlshortener
            </p>
        </div>
    </div>
</div>


